This is a simple RPG game written in Unity. The game features a player character that can move around and attack enemies by clicking the mouse. The goal of the game is to defeat all of the enemies.

## Controls:

* Click the mouse cursor in the desired direction to move the player character.
* Click on an enemy to attack it.
## Tips:

* Be careful not to get hit by enemies.
* Try to stay behind enemies so that they cannot attack you.
* Use the environment to your advantage. For example, you can hide behind objects to avoid attacks.
## Known Issues:

* The game is still under development, so there may be some bugs or glitches.
* The game balance may not be perfect.
### Feedback:

If you have any feedback or suggestions for the game, please feel free to share them with the developers.
