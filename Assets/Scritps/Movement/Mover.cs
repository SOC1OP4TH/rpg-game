using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using RPG.Combat;
using RPG.Core;
namespace RPG.Movement{
public class Mover : MonoBehaviour, IAction
{
    // Start is called before the first frame update
    Health health;
    NavMeshAgent meshAgent;
    void Start()
    {
        meshAgent = GetComponent<NavMeshAgent>();
        health = GetComponent<Health>();
    }

    // Update is called once per frame
    void Update()
    {
    meshAgent.enabled= !health.IsDead();
        UpdateAnimations();
    }
   public void MoveTo(Vector3 destination)
   {
        meshAgent.destination = destination;
        meshAgent.isStopped = false;
   }
     void UpdateAnimations()
  {
    Vector3 velocity = meshAgent.velocity;
    Vector3 localVelocity = transform.InverseTransformDirection(velocity);
    float speed = localVelocity.z;
    GetComponent<Animator>().SetFloat("forwardSpeed", speed);
  }
 public void StartMoveAction(Vector3 destination)
  {

    GetComponent<ActionScheduler>().StartAction(this);
    MoveTo(destination);

  }
  public void Cancel()
  {
    meshAgent.isStopped = true;
    //GetComponent<Fighter>().Cancel();
    Debug.Log("dayandi");
  }
  
}
}
