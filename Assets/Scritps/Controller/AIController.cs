using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RPG.Combat;
using RPG.Movement;
using   RPG.Core;
namespace RPG.Controller
{


public class AIController : MonoBehaviour
{
    //make enemies follow me
    float distance = 5f;
    Vector3 guardPosition;
    [SerializeField]
    float suspicionTime;
    float waypointTolerance = 1f;
    int currentWaypoint = 0;
    float dwellingTime=2f;
    float timeSinceLastSeen = Mathf.Infinity;
    float timeSinceArrivedWaypoint = Mathf.Infinity;
    [SerializeField]
    GameObject targetPlayer;
    Fighter fighter ;

    Mover move;
    [SerializeField]
    PatrolPath path;
    Health health;
    public bool isFollowing;


    void Start()
    {
        move = GetComponent<Mover>();
        guardPosition = transform.position;
        targetPlayer = GameObject.FindGameObjectWithTag("Player");
       fighter = GetComponent<Fighter>();
       health=GetComponent<Health>();
    }   
    void Update()
    {
        if(health.IsDead()) return;
        CheckDistance();
    }

    void CheckDistance()
    {
        if(IsFollowing()&&fighter.CanAttack(targetPlayer))
        {
          AttackBehaviour();
        }
        else if(timeSinceLastSeen<suspicionTime)
        {
           suspicionBehaviour();
        }
        else
        {
            PatrolBehaviour();
            
       // fighter.Cancel();

        }
      UpdateTimer();
    }
    void suspicionBehaviour()
    {
        GetComponent<ActionScheduler>().CancelCurrentAction();
    }
    void UpdateTimer()
    {
        timeSinceArrivedWaypoint+=Time.deltaTime;
        timeSinceLastSeen += Time.deltaTime;
    }
    void PatrolBehaviour()
    {
        Vector3 nextPosition = guardPosition;
        if(path!=null)
        {
            if(AtWayPoint())
            {
                timeSinceArrivedWaypoint = 0;
                CycleWaypoint();
          
            }
            nextPosition = GetCurrentWayPoint();

        }
        if(timeSinceArrivedWaypoint>dwellingTime)
        {
            move.StartMoveAction(nextPosition);
        }

    }
    void CycleWaypoint()
    {
     currentWaypoint = path.GetNextIndex(currentWaypoint);
    }
    Vector3 GetCurrentWayPoint()
    {
        return path.GetWayPoint(currentWaypoint);
    }
    bool AtWayPoint()
    {
        float distanceToWaypoint = Vector3.Distance(transform.position,path.GetWayPoint(currentWaypoint));
        return distanceToWaypoint<waypointTolerance;
    }
    void AttackBehaviour()
    {
        fighter.Attack(targetPlayer);
          timeSinceLastSeen = 0;
    }
    bool IsFollowing()
    {
        return distance>Vector3.Distance(targetPlayer.transform.position,this.transform.position);
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position,distance);

    }     
}
}
