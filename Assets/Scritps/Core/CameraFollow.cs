using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace RPG.Core{
public class CameraFollow : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]
    private Transform _target;
    Vector3 offset;

    void Start()
    {
        offset = transform.position - _target.position;
            
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = _target.position + offset;
    }
}
}
