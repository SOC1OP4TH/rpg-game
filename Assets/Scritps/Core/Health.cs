using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Health : MonoBehaviour
{
   bool dead;
  public bool IsDead()
  {
   return dead;
  }
   public float _health = 100;
   public void TakeDamage(float damage)
   {
    _health = Mathf.Max(_health-damage,0);
    Debug.Log(_health);
    if(_health==0)
    Die();
   }
   
   public void Die()
   {
      if(dead)
      return;
      dead=true;
      GetComponent<RPG.Core.ActionScheduler>().CancelCurrentAction();
      GetComponent<Animator>().SetTrigger("Die");
   }
}
