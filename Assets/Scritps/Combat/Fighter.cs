using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using   RPG.Movement;
using RPG.Core;
namespace RPG.Combat{
public class Fighter : MonoBehaviour, IAction
{
   [SerializeField]
   float weaponRange = 2;
   [SerializeField]
   float timeBetweenAttacks = 1f;
   Health _target;
   [SerializeField] float _damage=3;
   float timeSinceLastAttack=Mathf.Infinity;
   Animator animator;
   void Start()
   {
    animator = GetComponent<Animator>();
   }
   public bool CanAttack(GameObject combatTarget)
   {
    if(combatTarget==null)
        return false;
        Health testHealth = combatTarget.GetComponent<Health>();
    return (testHealth!=null && !testHealth.IsDead());
        
   }
   void AttackBehaviour()
   {
    if(timeSinceLastAttack > timeBetweenAttacks )
    {
        TriggerAttack();
        transform.LookAt(_target.transform.position);
        timeSinceLastAttack = 0;
    }
   }
  

   //aniamtion event
   void Hit()
   {
        _target?.TakeDamage(_damage);
        ///////////////////////////////////////////////////////////////////
        //float targetHealth = healthComponent._health;///////////////////
        // if (targetHealth<=0)   ///////////////////////////////////////
        // {////////////////////////////////////////////////////////////
        //         _target.GetComponent<Animator>().SetTrigger("Die");/
        //         Destroy(_target.gameObject,4);/////////////////////
        //         Cancel();/////////////////////////////////////////
        // }////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////
   }
   private bool GetIsInRange()
   {
    return Vector3.Distance(transform.position, _target.transform.position) <= weaponRange;
   }
    // Update is called once per frame
    void Update()
    {
        timeSinceLastAttack +=Time.deltaTime;
        if(_target==null) return;
        if(_target.IsDead()) return;
        if(!GetIsInRange())
        {
            GetComponent<Mover>().MoveTo(_target.transform.position);
            Debug.Log("going target");

        }
        else
        {
            GetComponent<Mover>().Cancel();
            AttackBehaviour();
        }

    }
   public void Attack(GameObject combatTarget)
    {
        Debug.Log(combatTarget.name);
          _target = combatTarget.GetComponent<Health>();
         //

          //fix this error   Cannot implicitly convert type 'UnityEngine.Transform' to 'Health' [Assembly-CSharp]


         GetComponent<ActionScheduler>().StartAction(this);
      
        Debug.Log("targetset");
        
    }
    void TriggerAttack()
   {
        animator.ResetTrigger("StopAttack");
        animator.SetTrigger("Attack");
   }
    void StopAttack()
    {
    animator.ResetTrigger("Attack");
    animator.SetTrigger("StopAttack");
    }
    // void AttackController(string state)
    // {
    //     animator.ResetTrigger(state);
    //     animator.SetTrigger(state);
    // }
    public void Cancel()
    {
        StopAttack();
        Debug.Log("Target itdi");

     _target = null;
    }
}
}
